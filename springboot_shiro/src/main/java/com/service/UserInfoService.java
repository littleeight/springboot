package com.service;

import com.model.UserInfo;

/**
 * Created by Administrator on 2016/12/13.
 */
public interface UserInfoService {

    /**通过username查找用户信息;*/
    public UserInfo findByUsername(String username);

}