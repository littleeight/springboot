package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootShiro1213Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootShiro1213Application.class, args);
	}
}
