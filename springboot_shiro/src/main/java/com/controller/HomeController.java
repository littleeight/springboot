package com.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

@RequestMapping({"/","/index"})
public String index(){
        return"/index";
        }

@RequestMapping(value="/login")
public String login(){
        return"login";
        }


        @RequestMapping(value="/userindex")
        @RequiresPermissions("123")//权限管理;
        public String userindex(){
                return"userindex";
        }

        @RequestMapping(value="/adminindex")
        @RequiresPermissions("admin")//权限管理;
        public String adminindex(){
                return"adminindex";
        }

        }