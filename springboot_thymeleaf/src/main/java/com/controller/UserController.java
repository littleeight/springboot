package com.controller;

import com.model.Topic;
import com.model.User;
import com.service.TopicService;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
@Controller
@RequestMapping("")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private TopicService topicService;

    @RequestMapping(value = "/getall",method = RequestMethod.GET)
    public String getAll(Model model){
        List<User> user=userService.findAll();
        model.addAttribute("users",user);
        return "list";
    }

    @RequestMapping(value = "/topic",method = RequestMethod.GET)
    public String getTopicAll(Model model){
        List<Topic> topics=topicService.findAll();
        model.addAttribute("topics",topics);
        return "topic";
    }
    @RequestMapping(value = "/check",method = RequestMethod.GET)
    public String checkTopic(HttpServletRequest request,Model model){
        String id=request.getParameter("id");
        String number=request.getParameter("number");
        model.addAttribute("id",id);
        Topic topic=new Topic();
        topic.setId(id);
        topic.setNumber(number);
        String result=topicService.check(topic);
        return result;
    }


}
