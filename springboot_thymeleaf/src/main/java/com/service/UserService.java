package com.service;

import com.model.User;

import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
public interface UserService {
    List<User> findAll();
}
