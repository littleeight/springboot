package com.service;

import com.model.Topic;

import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
public interface TopicService {
    List<Topic> findAll();
    String check(Topic topic);
}
