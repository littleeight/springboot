package com.service;

import com.mapper.TopicMapper;
import com.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
@SuppressWarnings("SpringJavaAutowiringInspection")
@Service("topicService")
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicMapper topicMapper;
    @Override
    public List<Topic> findAll() {
        return topicMapper.findAll();
    }

    @Override
    public String check(Topic topic) {
        Topic topic1=topicMapper.findById(topic.getId());
        if (topic1==null||topic1.getNumber().equals("")){
            return "iderror";
        }
        if ((!topic1.getNumber().equals("0"))||topic.getNumber().equals("0")){
            return "numbererror";
        }
        topicMapper.update(topic);
        return "success";
    }
}
