package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootThymeleaf1209Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootThymeleaf1209Application.class, args);
	}
}
