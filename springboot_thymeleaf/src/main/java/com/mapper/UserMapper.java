package com.mapper;

import com.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
@Mapper
public interface UserMapper {
    User findById(String id);
    User findByUsername(String username);
    List<User> findAll();
}
