package com.mapper;

import com.model.Topic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
@Mapper
public interface TopicMapper {
   List<Topic> findAll();
   Topic findById(String id);
   void update(Topic topic);
}
