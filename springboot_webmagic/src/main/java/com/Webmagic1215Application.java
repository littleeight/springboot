package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Webmagic1215Application {

	public static void main(String[] args) {
		SpringApplication.run(Webmagic1215Application.class, args);
	}
}
