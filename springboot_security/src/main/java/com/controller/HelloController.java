package com.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }


    @RequestMapping(value = "/helloadmin")
    //对应数据库中role为“ROLE_admin”
    @PreAuthorize("hasAnyRole('admin')")
    public String helloAdmin(){
        return "helloadmin";
    }

    @RequestMapping(value = "/hellouser")
    @PreAuthorize("hasAnyRole('admin', 'user')")
    public String helloUser(){
        return "hellouser";
    }
}