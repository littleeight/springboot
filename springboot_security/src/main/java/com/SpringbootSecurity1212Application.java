package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;

@SpringBootApplication
public class SpringbootSecurity1212Application {
	public static void main(String[] args) {
		SpringApplication.run(SpringbootSecurity1212Application.class, args);
	}
}
