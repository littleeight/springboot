package com.mapper;

import com.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator on 2016/12/9.
 */
@Mapper
public interface UserMapper {
    User selectById(String id);
    User selectByUsername(String username);
}
